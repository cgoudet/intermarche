import datetime
import itertools
import logging
from pathlib import Path

import holidays
import joblib
import numpy as np
import pandas as pd
from cgoudetcore.decorators import tracker
from cgoudetcore.mappers import AggSerieValue, DateFeaturer, FrameMapper
from cgoudetcore.utils import flatten_multiindex, reduce_float_type, reduce_int_type
from fastcore.utils import listify
from sklearn.preprocessing import OneHotEncoder
from umap import UMAP

REFDIR = Path(__file__).parent / ".." / ".." / "references"
DATADIR = Path(__file__).parent / ".." / ".." / "data" / "datasets"
ENCODIR = Path(__file__).parent / ".." / ".." / "data" / "encoders"


def add_pdv_infos(ventes):
    fn = DATADIR / "points_de_vente.csv"
    df = pd.read_csv(fn).rename(columns=lambda x: x.lower())
    for c in set(df.columns) - {"id_pdv"}:
        df[c] = df[c].str[-1]
    df = df.astype(int)
    return pd.merge(ventes, df, on="id_pdv", how="left")


def add_prix_vente(ventes):
    return (
        ventes.merge(
            pd.read_parquet(DATADIR / "prix_vente.parquet"),
            on=["id_artc", "id_pdv", "year", "quarter"],
            how="left",
        )
        .fillna({"price": -1})
        .astype({"price": int})
    )


def add_missing_periods_and_metric_weights(df):
    """
    Add entries with 0 quantity on missing dates after the first non null entry.
    Compute wether the missed value should be considered in the metrics.
    """
    all_dts = pd.Index(
        pd.date_range(df["date"].min(), df["date"].max() + datetime.timedelta(days=6)),
        name="date",
    )
    quantities = (
        pd.pivot_table(
            df,
            index=["date"],
            columns=["id_pdv", "id_artc"],
            values="qte",
            aggfunc=np.sum,
        )
        .reindex(all_dts)
        .fillna(0)
    )
    quantities[quantities.cumsum() == 0] = np.nan

    weights = np.minimum(quantities.rolling(7, min_periods=1).max(), 1)

    dtypes = {"sample_weight_metric": np.int8, **df.dtypes}
    out = (
        pd.concat({"qte": quantities, "sample_weight_metric": weights}, axis="columns")
        .stack([1, 2])
        .reset_index()
        .astype(dtypes)
    )
    return out


def add_lagged_date(ventes, offset):
    """
    Add a column with the corresponding lagged date.
    Days that are at the begining of the dataset use the future lag
    """
    ventes["date_lagged"] = ventes.date - offset
    ventes["date_lagged"] = np.where(
        (ventes.date < pd.Timestamp("2018-12-15"))
        & (ventes.date_lagged.dt.week > ventes.date.dt.week),
        ventes.date + offset,
        ventes.date_lagged,
    )
    return ventes


def add_datepart(df):
    date_featurer = DateFeaturer(
        attrs=[
            "dayofweek",
            "day",
            "month",
            "week",
            "quarter",
            "year",
            "cos_dayinyear",
            "sin_dayinyear",
        ]
    )
    return pd.concat([df, date_featurer.fit_transform(df[["date"]])], axis="columns")


def add_holidays(ventes):
    fr_holidays = holidays.CountryHoliday("FR")
    dt_range = pd.date_range(ventes.date.min(), ventes.date.max())
    dt_range = dt_range[pd.Series(dt_range).apply(lambda x: x in fr_holidays)]
    ventes["holidays"] = ventes.date.isin(dt_range)
    ventes["before_holidays"] = ventes.date.isin(
        dt_range - pd.offsets.DateOffset(days=1)
    )
    return ventes


@tracker(log_start=True)
def train_agregate_lagged(ventes, group_cols, aggs={}, force=False):
    encoder_fn = ENCODIR / "encoder_lagged_{}.joblib".format("_".join(group_cols))

    if encoder_fn.exists() and not force:
        return

    aggs = {k: listify(v) for k, v in aggs.items()}
    agregates = (
        ventes.drop(columns=["yearweek_lagged", "date_lagged"])
        .rename(columns={"yearweek": "yearweek_lagged", "date": "date_lagged"})
        .groupby(group_cols)
        .agg(aggs)
    )

    agregates.columns = [
        "_".join(["lagged_agg"] + group_cols + [x])
        for x in flatten_multiindex(agregates.columns)
    ]
    encoder = FrameMapper(agregates, main_cols=group_cols)
    joblib.dump(encoder, encoder_fn)


@tracker(log_start=True)
def train_agg_encodings(df, force=False):
    opts = dict(target="qte", date_col="date", num_entries=4, fillna=0)

    referenced = df[df.price >= 0]
    steps = [["id_pdv"], ["id_pdv", "id_artc"], ["id_artc"]]
    weeks = [False, True]
    target_fcts = [
        np.median,
        np.max,
        np.std,
    ]
    for step, week in itertools.product(steps, weeks):

        cols = step + (["dayofweek"] if week else [])
        encoder_fn = ENCODIR / "encoder_agg_{}.joblib".format("_".join(cols))
        logging.info({"fct": "train_agg_encodings", "encoder": encoder_fn.stem})
        if encoder_fn.exists() and not force:
            continue
        encoder = AggSerieValue(
            group_cols=cols, target_fct=target_fcts, prefix="encoder_agg", **opts
        )
        encoder.fit(referenced)
        joblib.dump(encoder, encoder_fn)


@tracker(log_start=True)
def train_sous_famille_encodings(ventes, force=False):
    encoder_fn = ENCODIR / "encoder_umap_lb_vent_sous_faml.joblib"
    if encoder_fn.exists() and not force:
        return
    artc_hierarchies = ventes[
        ["lb_vent_rayn", "lb_vent_faml", "lb_vent_sous_faml"]
    ].drop_duplicates()
    onehot = OneHotEncoder(sparse=False).fit_transform(artc_hierarchies)
    reducer = UMAP(random_state=56, n_neighbors=30, min_dist=1)
    encodings = reducer.fit_transform(onehot)

    encoder = FrameMapper(
        pd.DataFrame(
            encodings,
            index=pd.MultiIndex.from_frame(artc_hierarchies),
            columns=[f"lb_vent_sous_faml_umap{i}" for i in range(encodings.shape[1])],
        ),
        main_cols=list(artc_hierarchies.columns),
    )

    joblib.dump(encoder, encoder_fn)


def train_sell_period(ventes, column="id_artc", period="week", force=False):
    outname = f"fraction_qte_{column}_{period}"
    encoder_fn = ENCODIR / f"encoder_{outname}.joblib"
    logging.info({"fct": "train_sell_period", "encoder": encoder_fn.stem})
    if encoder_fn.exists() and not force:
        return

    artc_sell_period = pd.pivot_table(
        ventes,
        index=period,
        columns=column,
        aggfunc=np.sum,
        values="qte",
        fill_value=0,
    )
    artc_sell_period = (
        (artc_sell_period / artc_sell_period.sum())
        .stack()
        .to_frame()
        .rename(columns={0: outname})
    )
    encoder = FrameMapper(
        artc_sell_period, main_cols=list(artc_sell_period.index.names)
    )
    joblib.dump(encoder, encoder_fn)


def run_save_encodings(ventes, prefix="", force=False):
    encoders_fn = ENCODIR.glob("encoder_*.joblib")
    for fn in encoders_fn:
        logging.info({"encoder": fn.stem})
        outfn = ENCODIR / "{}.parquet".format(
            fn.stem.replace("encoder_", f"{prefix}encodings_")
        )
        if outfn.exists() and not force:
            continue
        encoder = joblib.load(fn)
        encodings = (
            encoder.transform(ventes).pipe(reduce_float_type).pipe(reduce_int_type)
        )
        encodings.columns = encoder.get_feature_names()
        print(encodings.columns)
        encodings.to_parquet(outfn)


@tracker(log_start=True)
def combined_features(ventes):
    ventes = ventes[ventes.price >= 0]
    ventes = ventes.rename(
        columns={c: c[8:] for c in ventes.columns if c.startswith("encoder_")}
    )
    # ventes["diff_median_id_pdv_id_artc_dayofweek"] = (
    #     ventes["qte"] - ventes["agg_id_pdv_id_artc_dayofweek_qte_median"]
    # )
    # ventes["pct_median_id_pdv_id_artc_dayofweek"] = ventes[
    #     "diff_median_id_pdv_id_artc_dayofweek"
    # ] / np.maximum(ventes["agg_id_pdv_id_artc_dayofweek_qte_median"], 1)
    # ventes["diff_log_median_id_pdv_id_artc_dayofweek"] = ventes["qte_log1p"] - np.log1p(
    #     ventes["agg_id_pdv_id_artc_dayofweek_qte_median"]
    # )
    # ventes["pct_log_median_id_pdv_id_artc_dayofweek"] = ventes[
    #     "diff_log_median_id_pdv_id_artc_dayofweek"
    # ] / np.log1p(np.maximum(ventes["agg_id_pdv_id_artc_dayofweek_qte_median"], 1))
    #
    # width = np.log1p(
    #     ventes["agg_id_pdv_id_artc_dayofweek_qte_third_quantile"]
    # ) - np.log1p(ventes["agg_id_pdv_id_artc_dayofweek_qte_first_quantile"])
    # ventes["z_score"] = (
    #     ventes["qte_log1p"]
    #     - np.log1p(ventes["agg_id_pdv_id_artc_dayofweek_qte_median"])
    # ) / width.where(width > 0, 1)
    # medians = [col for col in ventes.columns if col.endswith("_median")]
    # for col in medians:
    #     ventes[col[:-7] + "_width"] = ventes[col[:-7] + "_std"] / np.maximum(
    #         ventes[col], 1
    #     )
    # ventes = reduce_int_type(ventes)
    correlated = np.loadtxt(REFDIR / "correlated.txt", dtype=str)
    to_drop = set(["yearweek_lagged", "date_lagged", "yearweek"] + list(correlated))
    ventes = ventes.drop(columns=to_drop, errors="ignore")

    ventes = ventes.fillna(0)
    ventes = reduce_float_type(ventes)

    return ventes
