import json
import re
from pathlib import Path

import dask.dataframe as dd
import numpy as np
import pandas as pd
from cgoudetcore.decorators import tracker
from cgoudetcore.utils import category_to_json, reduce_float_type, reduce_int_type
from dask.distributed import Client

from .features import (
    ENCODIR,
    add_datepart,
    add_holidays,
    add_lagged_date,
    add_missing_periods_and_metric_weights,
    add_pdv_infos,
    add_prix_vente,
    combined_features,
    run_save_encodings,
    train_agg_encodings,
)

DATADIR = Path(__file__).parent / ".." / ".." / "data" / "datasets"
pd.options.mode.chained_assignment = None


@tracker(log_start=True)
def combine_sources():
    ventes = (
        pd.read_csv(
            DATADIR / "ventes_2018.csv.zip",
            parse_dates=["DATE"],
        )
        .astype({"DATE": "datetime64[D]"})
        .pipe(lambda x: x.rename(columns={col: col.lower() for col in x.columns}))
        .pipe(add_missing_periods_and_metric_weights)
        .pipe(add_datepart)
        .pipe(add_holidays)
        .pipe(add_prix_vente)
        .pipe(add_pdv_infos)
        .pipe(add_lagged_date, pd.offsets.DateOffset(days=7 * 13))
        .merge(
            pd.read_parquet(DATADIR / "nomenclature_produits.parquet"),
            on="id_artc",
            how="left",
        )
        .pipe(lambda df: df[df["date"] < pd.Timestamp("2019-01-01")])
        .assign(
            is_valid=lambda df: df.date >= pd.Timestamp("2018-10-01"),
            qte_log1p=lambda x: np.log1p(x.qte),
            ca=lambda x: np.where(x.price >= 0, x.price * x.qte, 0),
            yearweek=lambda x: x.date.dt.year * 100 + x.date.dt.week,
            yearweek_lagged=lambda x: x.date_lagged.dt.year * 100
            + x.date_lagged.dt.week,
        )
        .drop(columns=["year", "quarter"])
        .pipe(reduce_int_type)
        .pipe(reduce_float_type)
        .reset_index()
    )
    ventes.to_parquet(DATADIR / "ventes_combined_sources.parquet")


def format_nomenclature_produit():
    fn = DATADIR / "nomenclature_produits.csv"
    df = (
        pd.read_csv(fn)
        .rename(columns=lambda x: x.lower())
        .sort_values(["lb_vent_rayn", "lb_vent_faml", "lb_vent_sous_faml"])
    )
    for col in set(df.columns) - {"id_artc"}:
        mapping = category_to_json(df[col])
        with (DATADIR / f"{col}.json").open("w") as f:
            json.dump(mapping, f)
        df[col] = df[col].map(mapping)
    df = df.astype(int)
    df.to_parquet(fn.with_suffix(".parquet"))


def format_prix_vente():
    fn = DATADIR / "prix_vente.csv.zip"
    pat = re.compile(r"Entre ([0-9]*).*")

    df = (
        pd.read_csv(fn)
        .rename(columns=lambda x: x.lower())
        .rename(
            columns={"annee": "year", "trimestre": "quarter", "prix_unitaire": "price"}
        )
        .assign(price=lambda x: x.price.str.extract(pat).fillna(0))
        .astype(int)
    )
    df.to_parquet(DATADIR / "prix_vente.parquet")


def base_vente_to_dask():
    ventes = (
        dd.read_parquet(DATADIR / "ventes_combined_sources.parquet")
        .set_index("index")
        .repartition(500)
    )
    ventes.to_parquet(DATADIR / "ventes_combined_sources_dask.parquet")


@tracker(log_start=True)
def train_encodings(force=False):
    ventes = pd.read_parquet(DATADIR / "ventes_combined_sources.parquet")
    train_agg_encodings(ventes, force=force)
    # for source in ["id_pdv", "id_artc"]:
    #     train_sell_period(ventes, source, "week")
    #     train_sell_period(ventes, source, "dayofweek")
    # train_sell_period(ventes, "id_artc", "id_regn")
    # train_sell_period(ventes, "id_artc", "id_voct")
    # train_agregate_lagged(
    #     ventes, ["id_pdv", "id_artc", "yearweek_lagged"], {"qte": [np.sum]}, force=True
    # )
    # train_agregate_lagged(
    #     ventes,
    #     ["id_pdv", "yearweek_lagged"],
    #     {"id_artc": ["nunique"], "qte": [np.sum], "ca": [np.sum]},
    #     force=True,
    # )
    # train_agregate_lagged(
    #     ventes,
    #     ["id_artc", "yearweek_lagged"],
    #     {"id_pdv": ["nunique"], "qte": [np.sum]},
    #     force=True,
    # )
    # train_agregate_lagged(
    #     ventes,
    #     ["id_artc", "date_lagged"],
    #     {"qte": np.sum},
    # )
    # train_agregate_lagged(
    #     ventes,
    #     ["id_pdv", "date_lagged"],
    #     {"qte": np.sum, "ca": np.sum},
    # )
    # train_agregate_lagged(
    #     ventes,
    #     ["id_pdv", "id_artc", "date_lagged"],
    #     {"qte": np.sum},
    # )


@tracker(log_start=True)
def precompute_encodings(test=False, force=False):

    if test:
        base_fn = DATADIR / "test_base_ventes.parquet"
        ventes = pd.read_parquet(base_fn)
        prefix = "test_"
    else:
        ventes = dd.read_parquet(DATADIR / "ventes_combined_sources_dask.parquet")
        prefix = ""
    run_save_encodings(ventes, prefix, force=force)


def add_encodings():
    ventes = dd.read_parquet(DATADIR / "ventes_combined_sources_dask.parquet")
    encodings_fn = ENCODIR.glob("encodings_*.parquet")

    encodings = [dd.read_parquet(fn) for fn in encodings_fn]
    ventes = dd.concat([ventes] + encodings, axis="columns").repartition(500)
    ventes.to_parquet(
        DATADIR / "ventes_encodings_dask.parquet",
        coerce_timestamps="ms",
        schema="infer",
    )


def finalize_training():
    ventes = dd.read_parquet(DATADIR / "ventes_encodings_dask.parquet").pipe(
        combined_features
    )
    ventes.to_parquet(
        DATADIR / "ventes_final_dask.parquet",
    )


def pipeline():
    # format_prix_vente()
    # format_nomenclature_produit()
    # combine_sources()
    # base_vente_to_dask()
    # train_encodings(force=True)
    # precompute_encodings()
    # add_encodings()
    finalize_training()


if __name__ == "__main__":
    client = Client(n_workers=4)
    pipeline()
